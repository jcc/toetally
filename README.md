Welcome to TillyTally
======================

TillyTally is a Python script that uses PyGame to draw
cards for a tally system.

It can run either in an Xorg window or under SDL (full screen)
on a framebuffer without needing any additional configuration.

Dependencies: python3, python3-pygame

TillyTally is curently pre-alpha. When you run it (./tillytally.py),
you can press the following buttons to draw some test cards:

 - l: Draw "LIVE" test card
 - f: Draw "FREE" test card
 - s: Draw "STAND BY" test card
 - e: Draw "ERROR" test card
 - a: Draw "SETUP" test card
 - r: Draw "READY" test card
 - o: Draw "OVER TIME" test card

Plans
------

Probably going to integrate with voctomix-core, and add some additional buttons
to voctomix-gui.

If adding some extra UI to voctomix won't be possible/practical, an additional
tillytally control client might be necessary.
