#!/usr/bin/env python3
"""
TillyTally:

A simple script that draws cards in a
framebuffer or X window.
"""

import sys
import pygame
import os

pygame.init()
pygame.font.init()

# Some initial variables
# TODO: Get from config file
speed = [0, 0]
title_font = "Cantarell Extra Bold"
body_font = "Cantarell Bold"
# Set some colours
black = 0, 0, 0
grey = 40, 40, 40
light_grey = 100, 100, 100
red = 180, 38, 34
green = 37, 110, 51
cyan = 40, 200, 200
purp = 70, 10, 140
blue = 80, 80, 250

# Set up window, calculate some initial screen values
pygame.display.set_caption('Tilly Tally')

if os.environ['DISPLAY'] == "":
    # Run full-screen if we're not under X
    INITIAL_SCREEN_WIDTH = 0
    INITIAL_SCREEN_HEIGHT = 0
else:
    # Run in a window under X
    INITIAL_SCREEN_WIDTH = 800
    INITIAL_SCREEN_HEIGHT = 480

size = width, height = INITIAL_SCREEN_WIDTH, INITIAL_SCREEN_HEIGHT

if os.environ['DISPLAY'] == "":
    screen = pygame.display.set_mode((size), pygame.FULLSCREEN)
    pygame.mouse.set_visible(False)
else:
    screen = pygame.display.set_mode((size), pygame.NOFRAME)

SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_surface().get_size()


def clear_screen(colour):
    screen.fill(colour)


def drawcard(title, titlebgcolour, bgcolour,
             line_1, line_2, line_3, line_4, line_5):
    clear_screen(bgcolour)
    pygame.draw.rect(screen, (titlebgcolour), (50, 50, SCREEN_WIDTH-110, 110))
    titlefont = pygame.font.SysFont(title_font, 120)
    label = titlefont.render(title, 1, (255, 255, 255))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 105))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(body_font, 50)
    label = textfont.render(line_1, 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 220))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(body_font, 50)
    label = textfont.render(line_2, 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 270))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(body_font, 50)
    label = textfont.render(line_3, 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 320))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(body_font, 50)
    label = textfont.render(line_4, 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 370))
    screen.blit(label, text_rect)

    textfont = pygame.font.SysFont(body_font, 50)
    label = textfont.render(line_5, 1, (200, 200, 200))
    text_rect = label.get_rect(center=(SCREEN_WIDTH/2, 420))
    screen.blit(label, text_rect)

    pygame.display.flip()


# Draw a welcome card
drawcard("WELCOME", light_grey, grey,
         "",
         "Check README.md for keyboard shortcuts.",
         "",
         "Press a button (ex: 'f') to load a test card.",
         "")


# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if (event.key == pygame.K_ESCAPE) or (event.key == pygame.K_q):
                print("Escape pressed, exiting...")
                sys.exit()
            if (event.key == pygame.K_l):
                drawcard("LIVE", red, grey,
                         "This camera is on air.",
                         "",
                         "Please to not make any",
                         "sudden movements.",
                         "")
            if (event.key == pygame.K_f):
                drawcard("FREE", green, grey,
                         "This camera is not live.",
                         "",
                         "You may move it swiftly to an",
                         "appropriate position.",
                         "")
            if (event.key == pygame.K_s):
                drawcard("STAND BY", light_grey, grey,
                         "",
                         "No stream or recording is",
                         "currently taking place.",
                         "",
                         "")
            if (event.key == pygame.K_e):
                drawcard("ERROR", grey, red,
                         "Connection to director lost.",
                         "",
                         "Attempting to reconnect...",
                         "Move camera with caution.",
                         "")
            if (event.key == pygame.K_m):
                drawcard("MESSAGE", blue, grey,
                         "Message from director:",
                         "",
                         "Please zoom out slightly, an",
                         "additional speaker is expected to join.",
                         "")
            if (event.key == pygame.K_a):
                drawcard("SETUP", blue, grey,
                         "",
                         "My IP is 10.1.14.78.",
                         "I am ready to be set up.",
                         "",
                         "")
            if (event.key == pygame.K_r):
                drawcard("READY", purp, grey,
                         "",
                         "The video team is ready",
                         "to start the session.",
                         "",
                         "")
            if (event.key == pygame.K_o):
                drawcard("OVER TIME!", red, grey,
                         "Dear talkmeister,",
                         "",
                         "This session is over time.",
                         "Broadcast will end abruptly.",
                         "")

        if event.type == pygame.QUIT:
            sys.exit()
